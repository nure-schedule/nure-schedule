var mysql = require('mysql');
var nureApi = require('./../modules/nure_api');

var INSERT_AUDITORIES = 'INSERT INTO auditories (id, name, type, floor) VALUES ?';
var INSERT_FACULTIES = 'INSERT INTO faculties (id, shortName, fullName) VALUES ?';
var INSERT_DIRECTIONS = "INSERT INTO directions (id, shortName, fullName) VALUES ?";
var INSERT_GROUPS = "INSERT INTO `groups` (id, name, directionId) VALUES ?";
var INSERT_FACULTY_DIRECTION = "INSERT INTO faculty_direction (facultyId, directionId) VALUES ?";
var INSERT_DEPARTMENTS = "INSERT INTO departments (id, shortName, fullName, facultyId) VALUES ?";
var INSERT_TEACHERS = "INSERT INTO teachers (id, shortName, fullName) VALUES ?";
var INSERT_TEACHER_DEPARTMENT = "INSERT INTO teacher_department (teacherId, departmentId) VALUES ?";

var INSERT_SUBJECTS = "INSERT INTO subject (id, shortName, fullName) VALUES ?";
var INSERT_HOURS = "INSERT INTO hours (id, type, quantity, subject_id) VALUES ?"
var INSERT_HOURS_TEACHER = "INSERT INTO hours_teacher (id, teacher_id, hours_id) VALUES ?"
var INSERT_EVENT_GROPS = "INSERT INTO event_grops (id, event_id, group_id) VALUES ?";
var INSERT_EVENTS = "INSERT INTO events (id, typeOfClass, startTime, endTime, subject_id, auditory_id) VALEUS ?";


function getConnection() {
    return mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "root",
        database: "schedule"
    });
}

exports.cloneAuditories = function () {
    let con = getConnection();
    con.connect(function (err) {
        if (err) throw err;
        nureApi.getAuditories()
            .then(function (data) {
                var auditories = [];
                data.university.buildings.forEach(
                    b => b.auditories.forEach(a => auditories.push(a)));
                var arr = [];
                auditories.forEach(el =>
                    arr.push([el.id, el.short_name, el.auditory_types.map(type => type.short_name).toString(), el.floor]));

                con.query(INSERT_AUDITORIES, [arr], function (err, result) {
                    if (err) throw err;
                });
            }, function (err) {
                console.log(err);
            });
    });
}

exports.cloneStructureWithGroups = function () {
    let con = getConnection();
    con.connect(function (err) {
        if (err) throw err;
        nureApi.getStructureWithGroups()
            .then(function (data) {
                var faculties = [];
                var faculty_directions = [];
                var directions = [];
                var groups = {};
                data.university.faculties.forEach(f => {
                    faculties.push([f.id, f.full_name, f.short_name]);
                    f.directions.forEach(d => {
                        if (d.groups) {
                            d.groups.forEach(group => {
                                groups[group.id] = [group.id, group.name, d.id];
                            });
                        }
                        d.specialities.forEach(spec => {
                            spec.groups.forEach(group => {
                                groups[group.id] = [group.id, group.name, d.id];
                            });
                        });
                        if (!directions.some(el => el[0] == d.id)) {
                            directions.push([d.id, d.full_name, d.short_name]);
                        }
                        faculty_directions.push([f.id, d.id]);
                    });
                });
                var groupArr = [];
                Object.keys(groups).forEach(key => {
                    groupArr.push(groups[key]);
                });

                con.query(INSERT_FACULTIES, [faculties], function (err, result) {
                    if (err) throw err;
                });
                con.query(INSERT_DIRECTIONS, [directions], function (err, result) {
                    if (err) throw err;
                });
                con.query(INSERT_FACULTY_DIRECTION, [faculty_directions], function (err, result) {
                    if (err) throw err;
                });
                con.query(INSERT_GROUPS, [groupArr], function (err, result) {
                    if (err) throw err;
                });
            });
    });
}

exports.cloneStructureWithTeachers = function () {
    let con = getConnection();
    con.connect(function (err) {
        if (err) throw err;
        nureApi.getStructure()
            .then(function (data) {
                var teachers = [],
                    teacher_department = [],
                    departments = [];
                data.university.faculties.forEach(f => {
                    f.departments.forEach(d => {
                        departments.push([d.id, d.full_name, d.short_name, f.id]);
                        d.teachers.forEach(t => {
                            if (!teachers.some(el => el[0] === t.id)) {
                                teachers.push([t.id, t.short_name, t.full_name]);
                            }
                            teacher_department.push([t.id, d.id]);
                        });
                    });
                });
                con.query(INSERT_DEPARTMENTS, [departments], function (err, result) {
                    if (err) throw err;
                });
                con.query(INSERT_TEACHERS, [teachers], function (err, result) {
                    if (err) throw err;
                });
                con.query(INSERT_TEACHER_DEPARTMENT, [teacher_department], function (err, result) {
                    if (err) throw err;
                });
            });
    });
}

exports.cloneTeachers = function () {
    let con = getConnection();
    con.connect(function (err) {
        if (err) throw err;
        nureApi.getStructure()
            .then(function (data) {
                var teachers = [], teacher_department = [];
                data.university.faculties.forEach(f => {
                    f.departments.forEach(d => {
                        d.teachers.forEach(t => {
                            if (!teachers.some(el => el[0] === t.id)) {
                                teachers.push([t.id, t.short_name, t.full_name]);
                            }
                            teacher_department.push([t.id, d.id]);
                            console.log(d.id);
                        });
                    });
                });
                con.query(INSERT_TEACHERS, [teachers], function (err, result) {
                    if (err) throw err;
                });
                con.query(INSERT_TEACHER_DEPARTMENT, [teacher_department], function (err, result) {
                    if (err) throw err;
                });
            });
    });
}

exports.cloneEventsWithSubjects = function () {
    var groupIds //= getGroupIds();
    let con = getConnection();
    con.connect(function (err) {
        if (err) throw err;
        groupIds.forEach(groupId => {
            nureApi.getSchedule(groupId)
                .then(data => {

                });
        });
    });
}