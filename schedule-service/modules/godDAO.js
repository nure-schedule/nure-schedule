var mysql = require('mysql');

var GET_DIRECTIONS = 'SELECT shortName from directions'
var GET_GROUPS = "SELECT name from `groups` left join directions on `groups`.directionId = directions.id where directions.shortName = ? AND name REGEXP ?"
var GET_DEPARTMENTS = "SELECT departments.fullName from departments left join faculties on faculties.id = departments.facultyId where faculties.shortName = ?";
var GET_FACULTIES = "SELECT shortName from faculties";
var GET_TEACHERS = "SELECT fullName from teachers where id IN (select teacherId from teacher_department left join departments on teacher_department.departmentId = departments.id where departments.fullName = ?)"
var GET_TEACHER_ID_BY_NAME = "SELECT id from teachers where fullName = ?"
var GET_GROUP_ID_BY_NAME = "SELECT id from `groups` where name = ?"

function getConnection() {
    return mysql.createConnection({
        host: "epuakhaw0409",
        user: "valet",
        password: "valet",
        database: "schedule"
    });
}

async function syncQuery(query) {
    let con = getConnection();
    con.connect(err => {
        if (err) throw err;
    });
    return new Promise((resolve, reject) => {
        con.query(query, [].slice.call(arguments, 1), function (err, result) {
            if (err) reject(err);
            resolve(result);
        });
    });
}

exports.getDirections = async function () {
    let res = await syncQuery(GET_DIRECTIONS);
    return res.map(res => res.shortName);
}

exports.getFaculties = async function () {
    let res = await syncQuery(GET_FACULTIES);
    return res.map(res => res.shortName);
}

exports.getDepartments = async function (facultyName) {
    let res = await syncQuery(GET_DEPARTMENTS, facultyName);
    return res.map(res => res.fullName);
}

exports.getGroups = async function (directionName, course) {
    let res = await syncQuery(GET_GROUPS, directionName, '.+-' + (new Date().getFullYear() % 100 - course + 1) + '-.+');
    return res.map(res => res.name);
}

exports.getTeachers = async function (department) {
    let res = await syncQuery(GET_TEACHERS, department);
    return res.map(res => res.fullName);
}

exports.getGroupIdByName = async function (name) {
    let res = await syncQuery(GET_GROUP_ID_BY_NAME, name);
    return res[0].id;
}

exports.getTeacherIdByName = async function (name) {
    console.log(name)
    let res = await syncQuery(GET_TEACHER_ID_BY_NAME, name);
    console.log(res)
    return res[0].id;
}