var http = require('http');
var iconv = require('iconv-lite');

const API_ROOT = 'http://cist.nure.ua/ias/app/tt/';
const AUDITORIES = 'P_API_AUDITORIES_JSON';
const SCHEDULE_FOR_GROUP = 'P_API_EVENTS_GROUP_JSON';
const STRUCTURE_WITH_TEACHERS = "P_API_PODR_JSON";
const STRUCTURE_WITH_GROUPS = "P_API_GROUP_JSON";
const SCHEDULE_FOR_TEACHER = "P_API_EVENTS_TEACHER_JSON";

function sendRequest(proc, parameters) {
	let query = API_ROOT + proc + '?';
	query += Object.keys(parameters)
		.map((key) => key + '=' + parameters[key])
		.join('&');
	console.log(query)
	return new Promise((resolve, reject) => {
		http.get(query, res => {
			if (res.statusCode != 200) {
				reject("Bad status code: " + res.statusCode);
			}
			let chunks = [];
			res.on('data', (chunk) => {
				chunks.push(chunk);
			});
			res.on('end', () => {
				let body = iconv.decode(Buffer.concat(chunks), "win1251");
				body = body.replace('[\n}]', '[ ]');// -_-
				try {
					resolve(JSON.parse(body));
				} catch (e) {
					reject("Can not parse json");
				}
			});
		});
	});
}

exports.getAuditories = function () {
	return sendRequest(AUDITORIES, {});
}

exports.getStructure = function () {
	return sendRequest(STRUCTURE_WITH_TEACHERS, {});
}

exports.getStructureWithGroups = function () {
	return sendRequest(STRUCTURE_WITH_GROUPS, {});
}

exports.getSchedule = async function (id, fromDate, toDate, forTeacher) {

	var parameters = {};
	parameters[forTeacher ? 'p_id_teacher' : 'p_id_group'] = id;
	if (fromDate && toDate) {
		parameters.time_from = ~~(fromDate.valueOf() / 1000);
		parameters.time_to = ~~(toDate.valueOf() / 1000);
	}
	var json = await sendRequest(forTeacher ? SCHEDULE_FOR_TEACHER : SCHEDULE_FOR_GROUP, parameters);
	json.events.forEach(event => {
		event.subject = json.subjects.filter(sub => sub.id == event.subject_id)[0];
		event.type = json.types.filter(type => type.id == event.type)[0];
		event.groups = json.groups.filter(group => event.groups.some(group_id => group_id == group.id));
		event.teachers = json.teachers.filter(teachers => event.teachers.some(teachers_id => teachers_id == teachers.id));

		delete event.subject_id;

		let startDate = new Date(0);
		let endDate = new Date(0);
		startDate.setUTCSeconds(event.start_time);
		endDate.setUTCSeconds(event.end_time);
		event.start_time = startDate;
		event.end_time = endDate;
	});
	return json.events;
}

