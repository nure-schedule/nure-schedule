var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var cors = require('cors')

var scheduleRouter = require('./routes/scheduler');
var availabilityRouter = require('./routes/availability');
var departmentsRouter = require('./routes/departments');
var facultiesRouter = require('./routes/faculties');
var groupsRouter = require('./routes/groups');
var accountRouter = require('./routes/account');
var userRouter = require('./routes/user');
var authRouter = require('./routes/auth').router;

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    secret: 'nure-schedule',
    resave: true,
    saveUninitialized: true
}));

//app.use(function(req, res, next) {
//    res.header('Access-Control-Allow-Origin', '*');
//    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//    next();
//});
app.use(cors({
    origin: "http://localhost:8010",
    credentials: true
}))

app.use('/schedule', scheduleRouter);
app.use('/groups', groupsRouter);
app.use('/departments', departmentsRouter);
app.use('/faculties', facultiesRouter);
app.use('/availability', availabilityRouter);
app.use('/account', accountRouter);
app.use('/auth', authRouter);
app.use('/user', userRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    //
});

module.exports = app;
