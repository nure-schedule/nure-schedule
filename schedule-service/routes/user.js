var express = require("express");
const {google} = require('googleapis');
var router = express.Router();
var auth = require('./auth');

router.get('/info', async (req, resp) => {
    let oauth2Client = auth.getOAuthClient();
    oauth2Client.setCredentials(req.session['tokens']);
    var oauth2 = google.oauth2({
        auth: oauth2Client,
        version: 'v2'
    });
    oauth2.userinfo.get((err, res) => {
        if (err) {
            console.log(err);
            resp.send(err);
        } else {
            resp.send(res.data);
        }
    });
});

module.exports = router;