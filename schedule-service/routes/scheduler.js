var express = require('express');
var router = express.Router();
var dao = require('../modules/godDAO')
var nureApi = require('./../modules/nure_api');

router.get('/', async function (req, res, next) {
  var name = req.query.groupId;
  var fromDate = new Date(req.query.fromDate);
  var toDate = new Date(req.query.toDate);

  if (name.match(/.+-.+-.+/)) {
    var groupId = await dao.getGroupIdByName(name);
    res.send(JSON.stringify(await nureApi.getSchedule(groupId, fromDate, toDate)));
  } else {
    var teacherId = await dao.getTeacherIdByName(name);
    res.send(JSON.stringify(await nureApi.getSchedule(teacherId, fromDate, toDate, true)));
  }
});

module.exports = router;
