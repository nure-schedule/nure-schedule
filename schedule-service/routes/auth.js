var express = require("express");
const {google} = require('googleapis');
var router = express.Router();

let clientId = "719670800107-6g78bpucngu73oojp4u8rqvqld9jbls3.apps.googleusercontent.com";
let secretClient = "fXj8NykXr8wfpLFttUdW3iLv";
let rediectUrl = "http://localhost:8010/oauthcallback";

var OAuth2 = google.auth.OAuth2;

let scopes = [
    'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/userinfo.email'
];

let getOAuthClient = () => {
    return new OAuth2(clientId, secretClient, rediectUrl);
}

router.get("/url", async (req, resp) => {
    var url = getOAuthClient().generateAuthUrl({
        access_type: 'offline',
        scope: scopes
    });
    resp.send(url);
});

router.post("/token", async (req, resp) => {
    let oauth2Client = getOAuthClient();
    oauth2Client.getToken(req.body.code, (err, tokens) => {
        if (err) {
            console.log(err);
            resp.send(err);
            return;
        }
        oauth2Client.setCredentials(tokens);
        req.session['tokens'] = tokens;

        resp.send(tokens);
    });
});

module.exports.getOAuthClient = getOAuthClient;
module.exports.router = router;