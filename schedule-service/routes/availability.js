var express = require('express');
var router = express.Router();
var dao = require('../modules/godDAO')

router.get('/students/', async function (req, res, next) {
    res.send(await dao.getDirections());
});

router.get('/students/:faculty/', async function (req, res, next) {
    res.send([1, 2, 3, 4, 5, 6]);
});

router.get('/students/:faculty/:course/', async function (req, res, next) {
    var faculty = req.params.faculty;
    var course = req.params.course;
    res.send(await dao.getGroups(faculty, parseInt(course)));
});

//Teachers

router.get('/teachers/', async function (req, res, next) {
    res.send(await dao.getFaculties());
});
router.get('/teachers/:faculty/', async function (req, res, next) {
    var faculty = req.params.faculty;
    res.send(await dao.getDepartments(faculty));
});
router.get('/teachers/:faculty/:department/', async function (req, res, next) {
    var department = req.params.department;
    res.send(await dao.getTeachers(department));
});

module.exports = router;
