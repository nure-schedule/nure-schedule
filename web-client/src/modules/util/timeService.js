(function (global) {
    'use strict';

    let timeSerice = {};

    timeSerice.getDaysRange = function (fromDate, toDate) {
        let days = [];
        let currentDate = new Date(fromDate);
        let dif = Math.round(toDate - fromDate);
        dif = Math.round(dif / 1000 / 60 / 60 / 24);
        for (let i = 0; i < dif; i++) {
            days.push(new Date(currentDate));
            currentDate.addDays(1);
        }
        return days;
    };

    timeSerice.getWeeksRange = function (fromDate, toDate) {
        let weeks = [];
        let currentDate = new Date(fromDate);
        let dif = Math.round(toDate - fromDate);
        dif = Math.round(dif / 1000 / 60 / 60 / 24 / 7);
        for (let i = 0; i < dif; i++) {
            weeks.push(currentDate.getWeek());
            currentDate.addDays(7);
        }
        return weeks;
    };

    Date.prototype.getWeek = function () {
        let week = [];
        week.push(new Date(this));

        let tomorrow = new Date(this);
        while (tomorrow.getDay() < 6) {
            tomorrow.setDate(tomorrow.getDate() + 1);
            week.push(new Date(tomorrow));
        }

        let yesterday = new Date(this);
        while (yesterday.getDay() > 0) {
            yesterday.setDate(yesterday.getDate() - 1);
            week.push(new Date(yesterday));
        }

        week.sort((date1, date2) => date1 - date2);
        return week;
    };

    Date.prototype.addDays = function (daysNumber) {
        this.setDate(this.getDate() + daysNumber);
    };

    if (typeof exports === 'object') {
        module.exports = timeSerice;
    } else {
        global.timeSerice = timeSerice;
    }
})(this);
