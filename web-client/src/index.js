import Vue from 'vue';
import AppContent from './components/app';
import VModal from 'vue-js-modal';
import Meta from 'vue-meta';

Vue.use(VModal);
Vue.use(Meta);

let App = Vue.extend(AppContent);
new App().$mount('app');
